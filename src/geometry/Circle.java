package geometry;

public class Circle implements Shape {
	private double radius;
	
	Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		return Math.PI * Math.pow(this.radius, 2);
	}
	
	public double getPerimeter() {
		return 2 * Math.PI * radius;
	}
}
