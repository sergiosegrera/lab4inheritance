package inheritance;

public class ElectronicBook extends Book {
	int numberBytes;
	
	ElectronicBook(String title, String author, int numberBytes) {
		super(title, author);
		this.numberBytes = numberBytes;
	}
	
	public String toString() {
		String fromBase = super.toString();
		return fromBase + ", Number of bytes: " + this.numberBytes;
	}
}
