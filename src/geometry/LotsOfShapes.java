package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(5.2, 5.8);
		shapes[1] = new Rectangle(3.2, 8.4);
		shapes[2] = new Circle(2.4);
		shapes[3] = new Circle(9.7);
		shapes[4] = new Square(4.5);
		
		for (int i = 0; i < shapes.length; i++) {
			System.out.println("[" + i + "] Area: " + shapes[i].getArea() + ", Perimeter: " + shapes[i].getPerimeter());
		}
	}

}
