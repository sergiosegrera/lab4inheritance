package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] books = new Book[5];
		books[0] = new Book("The Hunger Games", "Suzanne Collins");
		books[1] = new ElectronicBook("1984", "George Orwell", 1004);
		books[2] = new Book("The Hobbit", "J.R.R. Tolkien");
		books[3] = new ElectronicBook("Romeo and Juliet", "William Shakespeare", 500);
		books[4] = new ElectronicBook("Othello", "William Shakespeare", 625);
		
		for (int i = 0; i < books.length; i++) {
			System.out.println(books[i]);
		}
	}

}
